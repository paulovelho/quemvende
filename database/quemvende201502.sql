/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TRIGGER IF EXISTS `admin_create`;
DROP TRIGGER IF EXISTS `admin_update`;
CREATE TRIGGER `admin_create` BEFORE INSERT ON `admin` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `admin_update` BEFORE UPDATE ON `admin` FOR EACH ROW SET NEW.updated_at = NOW();

INSERT INTO admin (`email`, `password`) VALUES ('paulovelho@quemven.de', '228575aa6e83aa3fac4596684f1ac5d6');



# Dump of table tab_arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_arquivos`;

CREATE TABLE `tab_arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distribuidora_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `data_processado` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_arquivos_create`;
DROP TRIGGER IF EXISTS `tab_arquivos_update`;
CREATE TRIGGER `tab_arquivos_create` BEFORE INSERT ON `tab_arquivos` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_arquivos_update` BEFORE UPDATE ON `tab_arquivos` FOR EACH ROW SET NEW.updated_at = NOW();



# Dump of table tab_buscas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_buscas`;

 CREATE TABLE `tab_buscas` ( 
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT, 
  `busca` varchar(255), 
  `estabelecimento_id` int(11), 
  `ip` varchar(255), 
  `created_at` timestamp NOT NULL, 
  `updated_at` timestamp NOT NULL, 
  PRIMARY KEY (`id`) 
);


DROP TRIGGER IF EXISTS `tab_buscas_create`;
DROP TRIGGER IF EXISTS `tab_buscas_update`;
CREATE TRIGGER `tab_buscas_create` BEFORE INSERT ON `tab_buscas` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_buscas_update` BEFORE UPDATE ON `tab_buscas` FOR EACH ROW SET NEW.updated_at = NOW();




# Dump of table tab_categorias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_categorias`;

CREATE TABLE `tab_categorias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_categorias_create`;
DROP TRIGGER IF EXISTS `tab_categorias_update`;
CREATE TRIGGER `tab_categorias_create` BEFORE INSERT ON `tab_categorias` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_categorias_update` BEFORE UPDATE ON `tab_categorias` FOR EACH ROW SET NEW.updated_at = NOW();




# Dump of table tab_contato
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_contato`;

CREATE TABLE `tab_contato` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `origem` varchar(255) DEFAULT NULL,
  `email_from` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `distribuidora_id` int(11) DEFAULT NULL,
  `estabelecimento_id` int(11) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT '',
  `answer_to` int(11) NULL, 
  `mensagem` text, 
  `created_at` timestamp NOT NULL, 
  `updated_at` timestamp NOT NULL, 
  PRIMARY KEY (`id`) 
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_contato_create`;
DROP TRIGGER IF EXISTS `tab_contato_update`;
CREATE TRIGGER `tab_contato_create` BEFORE INSERT ON `tab_contato` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_contato_update` BEFORE UPDATE ON `tab_contato` FOR EACH ROW SET NEW.updated_at = NOW();


# Dump of table tab_distribuidoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_distribuidoras`;

CREATE TABLE `tab_distribuidoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `nome_clean` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT '',
  `endereco` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `areas` text DEFAULT NULL,
  `condicoes` text DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_distribuidoras_create`;
DROP TRIGGER IF EXISTS `tab_distribuidoras_update`;
CREATE TRIGGER `tab_distribuidoras_create` BEFORE INSERT ON `tab_distribuidoras` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_distribuidoras_update` BEFORE UPDATE ON `tab_distribuidoras` FOR EACH ROW SET NEW.updated_at = NOW();

ALTER TABLE `tab_distribuidoras`
ADD FULLTEXT(`nome_clean`);



# Dump of table tab_estabelecimentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_estabelecimentos`;

CREATE TABLE `tab_estabelecimentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `contato` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_estabelecimentos_create`;
DROP TRIGGER IF EXISTS `tab_estabelecimentos_update`;
CREATE TRIGGER `tab_estabelecimentos_create` BEFORE INSERT ON `tab_estabelecimentos` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_estabelecimentos_update` BEFORE UPDATE ON `tab_estabelecimentos` FOR EACH ROW SET NEW.updated_at = NOW();



# Dump of table tab_estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_estados`;

CREATE TABLE `tab_estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_estados_create`;
DROP TRIGGER IF EXISTS `tab_estados_update`;
CREATE TRIGGER `tab_estados_create` BEFORE INSERT ON `tab_estados` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_estados_update` BEFORE UPDATE ON `tab_estados` FOR EACH ROW SET NEW.updated_at = NOW();


# Dump of table tab_produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_produtos`;

CREATE TABLE `tab_produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distribuidora_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `nome_clean` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `estilo` varchar(255) DEFAULT NULL,
  `estilo_clean` varchar(255) DEFAULT NULL,
  `envase` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `arquivo_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_produtos_create`;
DROP TRIGGER IF EXISTS `tab_produtos_update`;
CREATE TRIGGER `tab_produtos_create` BEFORE INSERT ON `tab_produtos` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_produtos_update` BEFORE UPDATE ON `tab_produtos` FOR EACH ROW SET NEW.updated_at = NOW();



# Dump of table tab_visitantes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_visitantes`;

CREATE TABLE `tab_visitantes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS `tab_visitantes_create`;
DROP TRIGGER IF EXISTS `tab_visitantes_update`;
CREATE TRIGGER `tab_visitantes_create` BEFORE INSERT ON `tab_visitantes` FOR EACH ROW SET NEW.created_at = NOW(), NEW.updated_at = NOW();
CREATE TRIGGER `tab_visitantes_update` BEFORE UPDATE ON `tab_visitantes` FOR EACH ROW SET NEW.updated_at = NOW();




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



#
# TAB_ESTADOS
# ------------------------------------------------------------

INSERT INTO tab_estados (`nome`) VALUES ('Acre');
INSERT INTO tab_estados (`nome`) VALUES ('Alagoas');
INSERT INTO tab_estados (`nome`) VALUES ('Amapá');
INSERT INTO tab_estados (`nome`) VALUES ('Amazonas');
INSERT INTO tab_estados (`nome`) VALUES ('Bahia');
INSERT INTO tab_estados (`nome`) VALUES ('Ceará');
INSERT INTO tab_estados (`nome`) VALUES ('Distrito Federal');
INSERT INTO tab_estados (`nome`) VALUES ('Espírito Santo');
INSERT INTO tab_estados (`nome`) VALUES ('Goiás');
INSERT INTO tab_estados (`nome`) VALUES ('Maranhão');
INSERT INTO tab_estados (`nome`) VALUES ('Mato Grosso');
INSERT INTO tab_estados (`nome`) VALUES ('Mato Grosso do Sul');
INSERT INTO tab_estados (`nome`) VALUES ('Minas Gerais');
INSERT INTO tab_estados (`nome`) VALUES ('Pará');
INSERT INTO tab_estados (`nome`) VALUES ('Paraíba');
INSERT INTO tab_estados (`nome`) VALUES ('Paraná');
INSERT INTO tab_estados (`nome`) VALUES ('Pernambuco');
INSERT INTO tab_estados (`nome`) VALUES ('Piauí');
INSERT INTO tab_estados (`nome`) VALUES ('Rio de Janeiro');
INSERT INTO tab_estados (`nome`) VALUES ('Rio Grande do Norte');
INSERT INTO tab_estados (`nome`) VALUES ('Rio Grande do Sul');
INSERT INTO tab_estados (`nome`) VALUES ('Rondônia');
INSERT INTO tab_estados (`nome`) VALUES ('Roraima');
INSERT INTO tab_estados (`nome`) VALUES ('Santa Catarina');
INSERT INTO tab_estados (`nome`) VALUES ('São Paulo');
INSERT INTO tab_estados (`nome`) VALUES ('Sergipe');
INSERT INTO tab_estados (`nome`) VALUES ('Tocantins');


INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('1','Cerveja','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('2','Cachaça','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('3','Vinho','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('4','Destilado','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('5','Água','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('6','Refrigerante','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('7','Suco','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('8','Espumante','1');
INSERT INTO tab_categorias (`id`,`nome`,`active`) VALUES ('9','Outro','1');




