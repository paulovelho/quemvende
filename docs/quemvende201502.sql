# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.61)
# Database: quemvende
# Generation Time: 2015-02-19 20:55:44 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tab_arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_arquivos`;

CREATE TABLE `tab_arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distribuidora_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `data_processado` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_categorias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_categorias`;

CREATE TABLE `tab_categorias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `nome_clean` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_contato
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_contato`;

CREATE TABLE `tab_contato` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email_from` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `distribuidora_id` int(11) DEFAULT NULL,
  `estabelecimento_id` int(11) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT '',
  `mensagem` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_distribuidoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_distribuidoras`;

CREATE TABLE `tab_distribuidoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT '',
  `endereco` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_estabelecimentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_estabelecimentos`;

CREATE TABLE `tab_estabelecimentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_estados`;

CREATE TABLE `tab_estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_produtos`;

CREATE TABLE `tab_produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distribuidora_id` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `estilo` varchar(255) DEFAULT NULL,
  `envase` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tab_visitantes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab_visitantes`;

CREATE TABLE `tab_visitantes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




/* ESTADOS: */
INSERT INTO tab_estados (`nome`) VALUES ('Acre');
INSERT INTO tab_estados (`nome`) VALUES ('Alagoas');
INSERT INTO tab_estados (`nome`) VALUES ('Amapá');
INSERT INTO tab_estados (`nome`) VALUES ('Amazonas');
INSERT INTO tab_estados (`nome`) VALUES ('Bahia');
INSERT INTO tab_estados (`nome`) VALUES ('Ceará');
INSERT INTO tab_estados (`nome`) VALUES ('Distrito Federal');
INSERT INTO tab_estados (`nome`) VALUES ('Espírito Santo');
INSERT INTO tab_estados (`nome`) VALUES ('Goiás');
INSERT INTO tab_estados (`nome`) VALUES ('Maranhão');
INSERT INTO tab_estados (`nome`) VALUES ('Mato Grosso');
INSERT INTO tab_estados (`nome`) VALUES ('Mato Grosso do Sul');
INSERT INTO tab_estados (`nome`) VALUES ('Minas Gerais');
INSERT INTO tab_estados (`nome`) VALUES ('Pará');
INSERT INTO tab_estados (`nome`) VALUES ('Paraíba');
INSERT INTO tab_estados (`nome`) VALUES ('Paraná');
INSERT INTO tab_estados (`nome`) VALUES ('Pernambuco');
INSERT INTO tab_estados (`nome`) VALUES ('Piauí');
INSERT INTO tab_estados (`nome`) VALUES ('Rio de Janeiro');
INSERT INTO tab_estados (`nome`) VALUES ('Rio Grande do Norte');
INSERT INTO tab_estados (`nome`) VALUES ('Rio Grande do Sul');
INSERT INTO tab_estados (`nome`) VALUES ('Rondônia');
INSERT INTO tab_estados (`nome`) VALUES ('Roraima');
INSERT INTO tab_estados (`nome`) VALUES ('Santa Catarina');
INSERT INTO tab_estados (`nome`) VALUES ('São Paulo');
INSERT INTO tab_estados (`nome`) VALUES ('Sergipe');
INSERT INTO tab_estados (`nome`) VALUES ('Tocantins');




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
