<?php

	include("../inc/global.php");

	MagratheaModel::IncludeAllModels();

	$s = new Short();
	$s->name = $_POST["name"];
	$s->url = $_POST["url"];

	$shorts = new ShortControl();
	$shorts->Add($s);

	echo '<div class="alert alert-success" role="alert"><strong>Done!</strong> URL will be shortened!...</div>';

?>