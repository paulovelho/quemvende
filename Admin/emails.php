<?php

$mailBase = '<h2 style="color: #FFCC00; font-family: Arial">QuemVende?</h2>
O <span style="color: #FFCC00; font-weight: bold;">QuemVende?</span> é um novo sistema que serve como vitrine para seus produtos
<br/>
O sistema é todo pensado principalmente para distribuidoras de bebidas. Com o aumento do número da demanda por cervejas especiais, vinhos e cachaças artesanais, se tornou um problema para certos estabelecimentos encontrarem o produto ideal para seu negócio. Nosso intuito é facilitar esse processo e incrementar as vendas.
<br/>
Nosso único objetivo é ajudar distribuidores e estabelecimentos a se encontrarem. Fazemos isso de forma totalmente gratuita a ambos os lados, e vamos continuar trabalhando para criar um grande banco de dados de distribuidoras e seus produtos.
<br/>';

?>


<div class="row-fluid">
	<div class="span3 right" style="padding-top: 5px;">
		<label for="destinatario" class="control-label">Destinatário:</label>
	</div>
	<div class="span9">
		<input type="text" name="destinatario" id="destinatario" placeholder="cliente@quemven.de" />
	</div>
</div>

<div class="row-fluid">
	<div class="span3 right" style="padding-top: 5px;">
		<label for="destinatario" class="control-label">Assunto:</label>
	</div>
	<div class="span9">
		<input type="text" name="assunto" id="assunto" placeholder="Conheça o QuemVende" />
	</div>
</div>

<div class="row-fluid">
	<textarea class="mailtxt" name="mail" id="mail" onKeyUp="Preview();"><?=$mailBase?></textarea>
</div>

<div class="row-fluid preview">
	<div class="span12">
		<span>Preview:</span>
		<div id="preview-box"></div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12" id="mail-response"></div>
</div>

<div class="row-fluid">
	<div class="span12 right">
		<button id="sendbtn" class="btn btn-success" onClick="SendAdminMail();">
			<i class="fa fa-envelope"></i> Enviar
		</button>
	</div>
</div>




<script type="text/javascript">
function Preview(){
	var html = $("#mail").val();
	$("#preview-box").html(html);
}
function SendAdminMail(){
	var data = {
		assunto: $("#assunto").val(),
		email_to: $("#destinatario").val(),
		msg: $("#mail").val()
	};
	$("#sendbtn").hide();
	MagratheaPost("emailsend.php", data, function(data){
		$("#mail-response").html(data);
	});
	window.setTimeout(function(){
		$("#sendbtn").show("slow");
	}, 3000);
}
Preview();
</script>