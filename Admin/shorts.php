<?php
	$sControl = new ShortControl();
	$shorts = $sControl->GetAllShorts();
?>
<div class="row-fluid">
	<div class="span12 mag_section">
		<header><h3><i class="fa fa-bars"></i> Shorts</h3>
		</header>
		<content>
			<div class="row-fluid">
				<div class="span6 right">
					Name:
				</div>
				<div class="span6">
					<input type="text" id="name" />
				</div>
			</div>

			<div class="row-fluid">
				<div class="span6 right">
					URL:
				</div>
				<div class="span6">
					<input type="text" id="url" />
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12 right">
					<button class="btn btn-primary" onClick="add();">
						<i class="fa fa-plus-circle"></i> Adicionar
					</button>
					<br/><br/>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12" id="admin-response"></div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<table class="table table-striped">
						<tr>
							<th>Name</th>
							<th>URL</th>
							<th>Clicks</th>
							<th>&nbsp;</th>
						</tr>
						<? foreach ($shorts as $s) {
							?>
							<tr>
								<td><?=$s->name?></td>
								<td><?=$s->url?></td>
								<td><?=$s->clicks?></td>
								<td><i class="fa fa-trash-o ibt" onClick="removeShort('<?=$s->name?>');"></i></td>
							</tr>
							<?
						} ?>
					</table>
				</div>
			</div>
		</content>
	</div>
</div>


<script type="text/javascript">
function add(){
	var name = $("#name").val();
	var url = $("#url").val();
	if(!name || !url){
		$("#admin-response").html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Name and URL should not be empty!</div>')
	}
	var page = "short_add.php";
	MagratheaPost(page, { name: name, url: url }, function(data){
		$("#admin-response").html(data);
	});
}
function removeShort(n){
	var page = "shorts_remove.php";
	MagratheaPost(page, { name: n }, function(data){
		$("#admin-response").html(data);
	});
}
</script>