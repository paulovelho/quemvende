<?php

$id = $_GET["id"];
$d = new Distribuidora($id);

if(@$_POST){
	include_once("Services/ClearString.php");
	$d = new Distribuidora($_POST["id"]);
	$d->SetNome($_POST["nome"]);
	$d->SetSlug($_POST["slug"]);
	$d->email = $_POST["email"];
	$d->endereco = $_POST["endereco"];
	$d->cidade = $_POST["cidade"];
	$d->estado_id = $_POST["estado"];
	$d->views = 0;
	$d->Save();
	die(true);
}

?>

<div class="span6">
	<form class="form-horizontal" id="addForm" method="post" onSubmit="return false;">
	<input type="hidden" name="id" value="<?=$d->id?>" />
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">nome</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="nome" name="nome" value="<?=$d->nome?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="slug" class="col-sm-2 control-label">slug</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="slug" name="slug" value="<?=$d->slug?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="email" class="col-sm-2 control-label">e-mail</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="email" name="email" value="<?=$d->email?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="endereco" class="col-sm-2 control-label">endereço</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="endereco" name="endereco" value="<?=$d->endereco?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="cidade" class="col-sm-2 control-label">cidade</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="cidade" name="cidade" value="<?=$d->cidade?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="estado" class="col-sm-2 control-label">estado</label>
		</div>
		<div class="span8 left">
			<?=EstadoControl::GetSelectEstados("estado", "", (!empty($d->id)?$d->estado_id:0) )?>
		</div>
	</div>
	<? if(!empty($d->id)) {
		?>
		<div class="row-fluid">
			<div class="span4 right">
				<label class="col-sm-2 control-label">reset password</label>
			</div>
			<div class="span8 left" id="reset_result">
				<button class="btn btn-warning" onClick="resetpassword(<?=$d->id?>);">
					<i class="fa fa-recycle"></i> Reset
				</button>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span4 right">
				<label class="col-sm-2 control-label">last login</label>
			</div>
			<div class="span8 left">
				<?=$d->last_login?>		
			</div>
		</div>
		<?
	}
	?>
	<div class="row-fluid">
		<div class="span8">&nbsp;</div>
		<div class="span4">
			<button class="btn btn-primary" onClick="add();">
				<i class="fa fa-check"></i> <?=(!empty($d->id)?"Editar":"Adicionar")?>
			</button>
		</div>
	</div>
	</form>
</div>

<script type="text/javascript">
function add(){
	MagratheaPost("distribuidora_add.php", $("#addForm").serializeObject(), function(data){
		console.info(data);
		MagratheaPost("distribuidora_list.php");
		ColorBoxDo("close");
	});
}
function resetpassword(id){
	if(confirm("Reiniciar a senha da [<?=$d->nome?>]?")){
		MagratheaPost("distribuidora_reset.php", { id: id }, function(data){
			$("#reset_result").html(data);
		});
	}
}
</script>
