<?php

	$contato = new Contato();
	$contato->origem = "admin";
	$contato->assunto = $_POST["assunto"];
	$contato->email_to = $_POST["email_to"];
	$contato->mensagem = $_POST["msg"];
	if($contato->AdminSend()){
		echo '<div class="alert alert-success" role="alert"><strong>Done!</strong> E-mail enviado com sucesso!</div>';
	} else {
		echo '<div class="alert alert-danger" role="alert"><strong>Erro!</strong> Houve erro no envio do e-mail!</div>';
	}

?>