<?php

$id = $_GET["id"];
$c = new Categoria($id);

if(@$_POST){
	$c = new Categoria($_POST["id"]);
	$c->SetNome($_POST["nome"]);
	$c->Save();
	die(true);
}

?>

<div class="span6">
	<form class="form-horizontal" id="addForm" method="post" onSubmit="return false;">
	<input type="hidden" name="id" value="<?=$c->id?>" />
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">Nome</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="nome" name="nome" value="<?=$c->nome?>">
		</div>
	</div>

	<div class="row-fluid">
		<div class="span8">&nbsp;</div>
		<div class="span4">
			<button class="btn btn-primary" onClick="add();">
				<i class="fa fa-check"></i> <?=(!empty($c->id)?"Editar":"Adicionar")?>
			</button>
		</div>
	</div>
	</form>
</div>

<script type="text/javascript">
function add(){
	MagratheaPost("categoria_edit.php", $("#addForm").serializeObject(), function(data){
		MagratheaPost("categoria_list.php");
		ColorBoxDo("close");
	});
}
</script>
