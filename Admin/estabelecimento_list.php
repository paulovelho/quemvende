<?php
	$dists = EstabelecimentoControl::GetAll();
?>
<div class="row-fluid">
	<div class="span12 mag_section">
		<header><h3><i class="fa fa-beer"></i> Estabelecimentos</h3>
		</header>
		<content>
			<!--div class="row-fluid">
				<div class="span12 right">
					<button class="btn btn-primary" onClick="add();">
						<i class="fa fa-plus-circle"></i> Adicionar
					</button>
					<br/><br/>
				</div>
			</div-->
			<div class="row-fluid">
				<div class="span12">
					<table class="table table-striped">
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Responsável</th>
							<th>E-mail</th>
							<th>&nbsp;</th>
						</tr>
						<? foreach ($dists as $d) {
							?>
							<tr>
								<td><?=$d->id?></td>
								<td><?=$d->nome?></td>
								<td><?=$d->contato?></td>
								<td><?=$d->email?></td>
								<td><i class="fa fa-eye ibt" onClick="view(<?=$d->id?>);"></i></td>
							</tr>
							<?
						} ?>
					</table>
				</div>
			</div>
		</content>
	</div>
</div>


<script type="text/javascript">
function view(id){
	var page = "estabelecimento_view.php&id="+id;
	ColorBox(page);
}
</script>