<?php
	$cats = CategoriaControl::GetAll();
?>
<div class="row-fluid">
	<div class="span12 mag_section">
		<header><h3><i class="fa fa-bars"></i> Categorias</h3>
		</header>
		<content>
			<div class="row-fluid">
				<div class="span12 right">
					<button class="btn btn-primary" onClick="add();">
						<i class="fa fa-plus-circle"></i> Adicionar
					</button>
					<br/><br/>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<table class="table table-striped">
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>&nbsp;</th>
						</tr>
						<? foreach ($cats as $c) {
							?>
							<tr>
								<td><?=$c->id?></td>
								<td><?=$c->nome?></td>
								<td><i class="fa fa-pencil ibt" onClick="edit(<?=$c->id?>);"></i></td>
							</tr>
							<?
						} ?>
					</table>
				</div>
			</div>
		</content>
	</div>
</div>


<script type="text/javascript">
function add(){
	var page = "categoria_edit.php";
	ColorBox(page);
}
function edit(id){
	var page = "categoria_edit.php&id="+id;
	ColorBox(page);
}
</script>