<?php

	include("../inc/global.php");

	MagratheaModel::IncludeAllModels();

	$shorts = new ShortControl();
	$shorts->Remove($_POST["name"]);

	echo '<div class="alert alert-success" role="alert"><strong>Done!</strong> URL for '.$_POST["name"].' will not longer be shortened!...</div>';

?>