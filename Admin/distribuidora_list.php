<?php
	$dists = DistribuidoraControl::GetAll();
?>
<div class="row-fluid">
	<div class="span12 mag_section">
		<header><h3><i class="fa fa-truck"></i> Distribuidoras</h3>
		</header>
		<content>
			<div class="row-fluid">
				<div class="span12 right">
					<button class="btn btn-primary" onClick="add();">
						<i class="fa fa-plus-circle"></i> Adicionar
					</button>
					<br/><br/>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<table class="table table-striped">
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Slug</th>
							<th>&nbsp;</th>
						</tr>
						<? foreach ($dists as $d) {
							?>
							<tr>
								<td><?=$d->id?></td>
								<td><?=$d->nome?></td>
								<td><?=$d->slug?></td>
								<td><i class="fa fa-pencil ibt" onClick="edit(<?=$d->id?>);"></i></td>
							</tr>
							<?
						} ?>
					</table>
				</div>
			</div>
		</content>
	</div>
</div>


<script type="text/javascript">
function add(){
	var page = "distribuidora_add.php";
	ColorBox(page);
}
function edit(id){
	var page = "distribuidora_add.php&id="+id;
	ColorBox(page);
}
</script>