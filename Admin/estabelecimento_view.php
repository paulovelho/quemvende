<?php

$id = $_GET["id"];
$e = new Estabelecimento($id);

?>

<div class="span6">
	<form class="form-horizontal" id="addForm" method="post" onSubmit="return false;">
	<input type="hidden" name="id" value="<?=$d->id?>" />
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">nome</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="nome" name="nome" value="<?=$e->nome?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">contato</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="contato" name="contato" value="<?=$e->contato?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">e-mail</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="email" name="email" value="<?=$e->email?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">endereço</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="endereco" name="endereco" value="<?=$e->endereco?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="nome" class="col-sm-2 control-label">cidade</label>
		</div>
		<div class="span8 left">
			<input type="text" class="form-control" id="cidade" name="cidade" value="<?=$e->cidade?>">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4 right">
			<label for="estado" class="col-sm-2 control-label">estado</label>
		</div>
		<div class="span8 left">
			<?=EstadoControl::GetSelectEstados("estado", "", (!empty($e->id)?$e->estado_id:0) )?>
		</div>
	</div>
	<? if(!empty($e->id)) {
		?>
		<div class="row-fluid">
			<div class="span4 right">
				<label class="col-sm-2 control-label">last login</label>
			</div>
			<div class="span8 left">
				<?=$e->last_login?>		
			</div>
		</div>
		<?
	}
	?>
	<div class="row-fluid" style="display: none;">
		<div class="span8">&nbsp;</div>
		<div class="span4">
			<button class="btn btn-primary" onClick="add();">
				<i class="fa fa-check"></i> <?=(!empty($e->id)?"Editar":"Adicionar")?>
			</button>
		</div>
	</div>
	</form>
</div>

