<?php

include(__DIR__."/Base/ContatoBase.php");

class Contato extends ContatoBase {
	public $responses = array();

	private function WsSend($postData){
		$wsContato = MagratheaConfig::Instance()->GetFromDefault("contact_server");

		$options = array(
			'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($postData),
			),
		);
		$context  = stream_context_create($options);
		$wsContato = $wsContato."/server.php?addMail";
		try {
			$result = file_get_contents($wsContato, false, $context);
			$res = json_decode($result);
			if($res->success)
				return true;
			else {
				Debug("Error sending mail: [".$res->error."] (".$res->message.")");
				return false;
			}
		} catch(Exception $ex){
			Debug($ex);
			return false;
		}
	}

	public function Send(){
		$this->Save();
		$auth = MagratheaConfig::Instance()->GetFromDefault("contact_auth");
		$source = MagratheaConfig::Instance()->GetFromDefault("contact_source");

		$postData = array(
			'auth' => $auth,
			'source' => $source,
			'to' => $this->email_to,
			'subject' => "QUEMVENDE? - [".$this->assunto."]",
			'message' => "Novo contato de [QUEMVENDE]: <br/><br/>".$this->mensagem,
			'priority' => 75 );
		return $this->WsSend($postData);
	}

	public function AdminSend(){
		$this->Save();
		$auth = MagratheaConfig::Instance()->GetFromDefault("contact_auth");
		$source = MagratheaConfig::Instance()->GetFromDefault("contact_source");

		$postData = array(
			'auth' => $auth,
			'source' => $source,
			'to' => $this->email_to,
			'subject' => $this->assunto,
			'message' => $this->mensagem,
			'priority' => 75 );
		return $this->WsSend($postData);
	}
}

class ContatoControl extends ContatoControlBase {
	public static function GetByEstabelecimento($est_id, $page=0, $perPage=10){
		$query = MagratheaQuery::Select()
			->Obj("Contato")
			->BelongsTo(new Distribuidora(), "distribuidora_id")
			->Where(array("estabelecimento_id" => $est_id))
			->OrderBy("tab_contato.created_at DESC")
			->Limit($perPage)
			->Page($page);
		return self::Run($query);
	}
	public static function GetByDistribuidora($dist_id, $page=0, $perPage=10){
		$query = MagratheaQuery::Select()
			->Obj("Contato")
			->BelongsTo(new Estabelecimento(), "estabelecimento_id")
			->Where(array("distribuidora_id" => $dist_id))
			->OrderBy("tab_contato.created_at DESC")
			->Limit($perPage)
			->Page($page);
		return self::Run($query);
	}

	public static function SendLembrarSenha($dest){

		$url = $dest->GetResetUrl();

		$mensagem = "Restauração de senha de QUEMVENDE: <br/><br/>";
		$mensagem .= "Você solicitou a restauração de senha.<br/>";
		$mensagem .= "Para resetar sua senha, acesse: <br/>";
		$mensagem .= $url."<br/><br/>";
		$mensagem .= "Caso você não tenha feito esta solicitação, desconsidere este e-mail.";

		$contato = new Contato();
		$contato->email_from = "contato@quemven.de";
		$contato->email_to = $dest->email;
		$contato->assunto = "Quem Vende? - Restauração de senha";
		$contato->mensagem = $mensagem;
		return $contato->Send();
	}
}

?>