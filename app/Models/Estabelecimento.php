<?php

include(__DIR__."/Base/EstabelecimentoBase.php");

class Estabelecimento extends EstabelecimentoBase {
	public function SetPassword($p){
		$this->password = md5($p);
	}
	public function GetResetUrl(){
		$url = MagratheaConfig::Instance()->GetFromDefault("url");
		return $url."/Estabelecimento/Welcome/".$this->id."/".md5($this->password."reset420");
	}
}

class EstabelecimentoControl extends EstabelecimentoControlBase {
	public static function Login($email, $password){
		$query = MagratheaQuery::Select()
			->Table("tab_estabelecimentos")
			->Where(array("email" => $email, "password" => md5($password)));
		$est = self::RunRow($query);
		if(!$est) return null;
		$est->last_login = now();
		$est->Save();
		return $est;
	}

	public static function GetByEmail($email){
		$query = MagratheaQuery::Select()
			->Table("tab_estabelecimentos")
			->Where(array("email" => $email));
		return self::RunRow($query);
	}

	public static function EmailExists($email){
		$est = self::GetByEmail($email);
		if($est) return true;
	}

}

?>