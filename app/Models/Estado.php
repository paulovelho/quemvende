<?php

include(__DIR__."/Base/EstadoBase.php");

class Estado extends EstadoBase {
	// your code goes here!
}

class EstadoControl extends EstadoControlBase {
	public static function GetSelectEstados($name, $class, $selected=0){
		$states = self::GetEstados();
		$html = "<select name='".$name."' id='".$name."' class='".$class."'>";
		foreach ($states as $st) {
			$html .= "<option value='".$st->id."' ".($selected == $st->id ? "selected" : "").">".$st->nome."</option>";
		}
		$html .= "</select>";
		return $html;
	}

	public static function GetEstados(){
		$queryStates = MagratheaQuery::Select("id, nome")->Object("Estado")->OrderBy("nome ASC");
		return self::Run($queryStates);
	}
	public static function GetEstadosArr(){
		$estados = [];
		$es = self::GetEstados();
		foreach ($es as $e){
			$estados[$e->id] = $e->nome;
		}
		return $estados;
	}

}

?>