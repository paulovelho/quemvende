<?php

include(__DIR__."/Base/CategoriaBase.php");

class Categoria extends CategoriaBase {
	public function SetNome($n){
		$this->nome = $n;
		return $this;
	}
}

class CategoriaControl extends CategoriaControlBase {
	public static function GetActive(){
		return self::Run(MagratheaQuery::Select()->Obj(new Categoria())->Where(array("active" => true))->OrderBy("nome"));
	}
}

?>