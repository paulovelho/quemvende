<?php

include(__DIR__."/Base/BuscaBase.php");

class Busca extends BuscaBase {
	public function Q($query){
		$this->busca = $query;
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$est = unserialize(@$_SESSION["estabelecimento"]);
		if($est)
			$this->estabelecimento_id = $est->id;
		$this->Insert();
	}
}

class BuscaControl extends BuscaControlBase {
	public static function SetEstabelecimento($est){
		$ip = $_SERVER['REMOTE_ADDR'];
		$query = MagratheaQuery::Update()
			->Table("tab_buscas")
			->Set( "estabelecimento_id", 4 )
			->Where(array("ip" => $ip, "estabelecimento_id"=> null));
		return self::Run($query);
	}
	public static function GetByEstabelecimento($est_id, $page=0, $perPage=10){
		$query = MagratheaQuery::Select()
			->Table("tab_buscas")
			->Where(array("estabelecimento_id" => $est_id))
			->OrderBy("created_at DESC")
			->Limit($perPage)
			->Page($page);
		return self::Run($query);
	}
}

?>