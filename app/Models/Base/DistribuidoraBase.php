<?php

## FILE GENERATED BY MAGRATHEA.
## SHOULD NOT BE CHANGED MANUALLY

class DistribuidoraBase extends MagratheaModel implements iMagratheaModel {

	public $id, $nome, $nome_clean, $slug, $email, $website, $password, $endereco, $cidade, $estado_id, $logo_id, $areas, $condicoes, $last_login, $active, $views;
	public $created_at, $updated_at;
	protected $autoload = array("Estado" => "estado_id");

	public function __construct(  $id=0  ){ 
		$this->Start();
		if( !empty($id) ){
			$pk = $this->dbPk;
			$this->$pk = $id;
			$this->GetById($id);
		}
	}
	public function Start(){
		$this->dbTable = "tab_distribuidoras";
		$this->dbPk = "id";
		$this->dbValues["id"] = "int";
		$this->dbValues["nome"] = "string";
		$this->dbValues["nome_clean"] = "string";
		$this->dbValues["slug"] = "string";
		$this->dbValues["email"] = "string";
		$this->dbValues["website"] = "string";
		$this->dbValues["password"] = "string";
		$this->dbValues["endereco"] = "string";
		$this->dbValues["cidade"] = "string";
		$this->dbValues["estado_id"] = "int";
		$this->dbValues["logo_id"] = "int";
		$this->dbValues["areas"] = "text";
		$this->dbValues["condicoes"] = "text";
		$this->dbValues["last_login"] = "datetime";
		$this->dbValues["active"] = "int";
		$this->dbValues["views"] = "int";

		$this->relations["properties"]["Arquivos"] = null;
		$this->relations["methods"]["Arquivos"] = "GetArquivos";
		$this->relations["lazyload"]["Arquivos"] = "true";
		$this->relations["properties"]["Estado"] = null;
		$this->relations["methods"]["Estado"] = "GetEstado";
		$this->relations["lazyload"]["Estado"] = "false";
		$this->relations["properties"]["Produtos"] = null;
		$this->relations["methods"]["Produtos"] = "GetProdutos";
		$this->relations["lazyload"]["Produtos"] = "true";
		$this->relations["properties"]["Contatos"] = null;
		$this->relations["methods"]["Contatos"] = "GetContatos";
		$this->relations["lazyload"]["Contatos"] = "true";

		$this->dbAlias["created_at"] =  "datetime";
		$this->dbAlias["updated_at"] =  "datetime";
	}

	// >>> relations:
	public function GetArquivos(){
		if($this->relations["properties"]["Arquivos"] != null) return $this->relations["properties"]["Arquivos"];
		$pk = $this->dbPk;
		$this->relations["properties"]["Arquivos"] = ArquivoControlBase::GetWhere(array("distribuidora_id" => $this->$pk));
		return $this->relations["properties"]["Arquivos"];
	}
	public function GetEstado(){
		if($this->relations["properties"]["Estado"] != null) return $this->relations["properties"]["Estado"];
		$this->relations["properties"]["Estado"] = new Estado($this->estado_id);
		return $this->relations["properties"]["Estado"];
	}
	public function SetEstado($estado){
		$this->relations["properties"]["Estado"] = $estado;
		$this->estado_id = $estado->GetID();
		return $this;
	}
	public function GetProdutos(){
		if($this->relations["properties"]["Produtos"] != null) return $this->relations["properties"]["Produtos"];
		$pk = $this->dbPk;
		$this->relations["properties"]["Produtos"] = ProdutoControlBase::GetWhere(array("distribuidora_id" => $this->$pk));
		return $this->relations["properties"]["Produtos"];
	}
	public function GetContatos(){
		if($this->relations["properties"]["Contatos"] != null) return $this->relations["properties"]["Contatos"];
		$pk = $this->dbPk;
		$this->relations["properties"]["Contatos"] = ContatoControlBase::GetWhere(array("distribuidora_id" => $this->$pk));
		return $this->relations["properties"]["Contatos"];
	}

}

class DistribuidoraControlBase extends MagratheaModelControl {
	protected static $modelName = "Distribuidora";
	protected static $dbTable = "tab_distribuidoras";
}
?>