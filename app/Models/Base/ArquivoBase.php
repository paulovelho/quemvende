<?php

## FILE GENERATED BY MAGRATHEA.
## SHOULD NOT BE CHANGED MANUALLY

class ArquivoBase extends MagratheaModel implements iMagratheaModel {

	public $id, $distribuidora_id, $nome, $data_processado;
	public $created_at, $updated_at;
	protected $autoload = null;

	public function __construct(  $id=0  ){ 
		$this->Start();
		if( !empty($id) ){
			$pk = $this->dbPk;
			$this->$pk = $id;
			$this->GetById($id);
		}
	}
	public function Start(){
		$this->dbTable = "tab_arquivos";
		$this->dbPk = "id";
		$this->dbValues["id"] = "int";
		$this->dbValues["distribuidora_id"] = "int";
		$this->dbValues["nome"] = "string";
		$this->dbValues["data_processado"] = "datetime";

		$this->relations["properties"]["Distribuidora"] = null;
		$this->relations["methods"]["Distribuidora"] = "GetDistribuidora";
		$this->relations["lazyload"]["Distribuidora"] = "true";

		$this->dbAlias["created_at"] =  "datetime";
		$this->dbAlias["updated_at"] =  "datetime";
	}

	// >>> relations:
	public function GetDistribuidora(){
		if($this->relations["properties"]["Distribuidora"] != null) return $this->relations["properties"]["Distribuidora"];
		$this->relations["properties"]["Distribuidora"] = new Distribuidora($this->distribuidora_id);
		return $this->relations["properties"]["Distribuidora"];
	}
	public function SetDistribuidora($distribuidora){
		$this->relations["properties"]["Distribuidora"] = $distribuidora;
		$this->distribuidora_id = $distribuidora->GetID();
		return $this;
	}

}

class ArquivoControlBase extends MagratheaModelControl {
	protected static $modelName = "Arquivo";
	protected static $dbTable = "tab_arquivos";
}
?>