<?php

include(__DIR__."/Base/DistribuidoraBase.php");

class Distribuidora extends DistribuidoraBase {
	public function SetNome($nome){
		$limpador = new ClearString();
		$this->nome = $nome;
		$clean = $limpador->clear_string($nome);
		$this->nome_clean = $clean;
	}
	public function SetPassword($p){
		$this->password = md5($p);
		$this->Save();
	}
	public function SetWebsite($url){
		if (preg_match("#https?://#", $url) === 0) {
			$url = "http://".$url;
		}
		$this->website = $url;
	}
	public function SetSlug($s){
		$limpador = new ClearString();
		$s = $limpador->cs_remove_accents($s);
		$this->slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $s)));
	}
	public function GetResetUrl(){
		$url = MagratheaConfig::Instance()->GetFromDefault("url");
		return $url."/Distribuidora/Welcome/".$this->slug."-".($this->id+420)."/".md5($this->password."reset420");
	}
	public function UpView(){
		$this->views = $this->views + 1;
		$this->Save();
	}
}

class DistribuidoraControl extends DistribuidoraControlBase {
	// and here!
	public static function GetBySlug($slug){
		$query = MagratheaQuery::Select()
			->Object(new Distribuidora())
			->BelongsTo(new Estado(), "estado_id")
			->Where(array("slug" => $slug));
		return self::Run($query, true);
	}
	public static function GetByEmail($email){
		$query = MagratheaQuery::Select()
			->Object(new Distribuidora())
			->Where(array("email" => $email));
		return self::Run($query, true);
	}

	public static function Login($email, $password){
		$query = MagratheaQuery::Select()
			->Table("tab_distribuidoras")
			->Where(array("email" => $email, "password" => md5($password)));
		$dist = self::RunRow($query);
		if(!$dist) return null;
		$dist->last_login = now();
		$dist->Save();
		return $dist;
	}

	public static function Search($query){
		$limpador = new ClearString();
		$q = $limpador->clear_string($query);
		str_replace("'", "\'", $q);

		$query = MagratheaQuery::Select()->Obj(new Distribuidora())
			->Where("nome_clean LIKE '%".$q."%'");
//		p_r($query->SQL());
		return self::Run($query);
	}
}

?>