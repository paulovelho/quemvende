<?php

include(__DIR__."/Base/ProdutoBase.php");

class Produto extends ProdutoBase {
	public function SetNome($n){
		$limpador = new ClearString();
		$this->nome = $n;
		$this->nome_clean = $limpador->clear_string($n);
		return $this;
	}
	public function SetEstilo($e){
		$limpador = new ClearString();
		$this->estilo = $e;
		$this->estilo_clean = $limpador->clear_string($e);
		return $this;
	}
}

class ProdutoControl extends ProdutoControlBase {

	public static function GetRandomName(){
		$magQuery = MagratheaQuery::Select("nome")
						->Obj(new Produto())
						->OrderBy("RAND()")
						->Limit(1);
		return self::QueryOne($magQuery->SQL());
	}

	public static function GetByDistribuidora($dist){
		$magQuery = MagratheaQuery::Select()->Obj(new Produto())
						->Where(array('distribuidora_id' => $dist))
						->HasOne(new Categoria(), "categoria_id")
						->OrderBy("tab_produtos.nome");
		return self::Run($magQuery);
	}

	public static function Search($query, &$total, $page=0, $perPage=10){
		$limpador = new ClearString();
		$q = $limpador->clear_string($query);
		str_replace("'", "\'", $q);

		$magQuery = MagratheaQuery::Select()->Obj(new Produto())
						->BelongsTo(new Distribuidora(), "distribuidora_id")
						->BelongsTo(new Categoria(), "categoria_id")
						->SelectExtra("MATCH (tab_produtos.nome_clean, tab_produtos.estilo_clean) AGAINST (\"*".$q."*\" IN BOOLEAN MODE) AS relevance")
						->Where("MATCH (tab_produtos.nome_clean, tab_produtos.estilo_clean) AGAINST (\"*".$q."*\" IN BOOLEAN MODE) ")
						->OrderBy("relevance DESC, tab_produtos.nome ASC");
		return self::RunPagination($magQuery, $total, $page, $perPage);
	}
}

?>