<?php
	include("inc/global.php");
	include($magrathea_path."/MagratheaAdmin.php"); // $magrathea_path should already be declared
 
	class LoginController extends MagratheaController {
		public static function Login(){
			self::GetSmarty()->display("login.html");
		}
	}

	if(!empty($_SESSION["user"])) {
		$admin = new MagratheaAdmin(); // adds the admin file
		$admin->title = "QuemVende - Magrathea Admin";
		try {
			$admin->addPlugin("colorbox");
		} catch (Exception $ex) {
			echo $ex->getMessage();
		}
		$admin->IncludeCSS("css/style.css");
		$admin->Load(); // load!
	} else {
		LoginController::Login();
	}
 
?>