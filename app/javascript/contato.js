function showContato(){
	$.ajax({
		url: "/Distribuidora/Contato",
		success: function(data){
			$("#contato-btn").slideUp("slow");
			$("#contato").html(data);
			$("#contato").slideDown("slow");
		}
	})
}

function CTLogin(){
	var post_data = {
		email: $(".loginbox #email").val(),
		password: $(".loginbox #password").val()
	};
	$("#ct_login_loading").show("slow");
	if(!post_data.email || !post_data.password){
		$("#ct_login_response").html("E-mail ou senha incorretos!");
		$("#ct_login_loading").hide();
		return;
	}
	$.ajax({
		url: "/Estabelecimento/Login", type: "POST", data: post_data,
		success: function(data){
			console.info(data);
			if( !data.success ){
				$("#ct_login_loading").hide();
				$("#ct_login_response").html(data.message);
			} else {
				$("#ct_login_loading").hide();
				$("#send_estab").val(data.estabelecimento.nome);
				$("#send_nome").val(data.estabelecimento.contato);
				$("#send_email").val(data.estabelecimento.email);
				$("#ct_estabelecimento_login").hide();
				$("#ct_estabelecimento_data").show();
				$("#send-row").slideDown("slow");
			}
		}
	});
}

function CTShowLogin(){
	$("#ct_estabelecimento").hide();
	$("#ct_estabelecimento_login").show();
}

function CTVoltar(){
	$("#ct_estabelecimento_login").hide();
	$("#ct_estabelecimento_cadastro").hide();
	$("#ct_estabelecimento").show();
}

function CTNextStep(){
	var form = $("#ct_firststep");
	form.validate({
		rules: {
			ct_email: {
				email: true
			}
		}
	});
	if(!form.valid()) return;
	$("#ct_estabelecimento").hide();
	$("#ct_estabelecimento_cadastro").show();
}

function CadastroRapido(){
	var form = $("#ct_cadastrorapido");
	form.validate({
		rules: {
			ct_password2: {
				equalTo: "#ct_password1"
			}
		}
	});
	if(!form.valid()) return;
	$("#ct_login_loading").show("slow");
	var post_data = {
		nome: $("#ct_nome").val(),
		email: $("#ct_email").val(),
		estab: $("#ct_estab").val(),
		estado: $("#ct_estado").val(),
		pass: $("#ct_password1").val()
	};
	$.ajax({
		url: "/Estabelecimento/CadastroRapido", type: "POST", data: post_data,
		success: function(data){
			console.info(data);
			$("#ct_login_loading").hide("slow");
			if( !data.success ){
				$("#ct_login_response").html(data.message);
			} else {
				$("#send_estab").val(post_data.estab);
				$("#send_nome").val(post_data.nome);
				$("#send_email").val(post_data.email);
				$("#ct_estabelecimento_cadastro").hide();
				$("#ct_estabelecimento_data").show();
				$("#send-row").slideDown("slow");
			}
		}
	});
}

function CTSend(){
	var form = $("#ct_mailform");
	form.validate({
		rules: {
			send_email: {
				email: true
			}
		}
	});
	if(!form.valid()) return;
	$("#ct_login_loading").show("slow");
	$("#send-row").slideUp("slow");
	var post_data = {
		dist_id: $("#dist-id").val(),
		estabelecimento: $("#send_estab").val(),
		responsavel: $("#send_nome").val(),
		email: $("#send_email").val(),
		assunto: $("#assunto").val(),
		message: $("#mensagem").val()
	}
	$.ajax({
		url: "/Contato/Interaction", type: "POST", data: post_data,
		success: function(data){
			console.info(data);
			$("#ct_login_loading").hide("slow");
			var html = "";
			var alert_class = "";
			if(data.success){
				alert_class = "alert-success";
				window.setTimeout(function(){
					$("#contato-btn").slideDown("slow");
					$("#contato").slideUp("slow");
				}, 5000);
			} else {
				alert_class = "alert-danger";
				$("#send-row").slideDown("slow");
			}
			html += '<div class="alert '+alert_class+'" role="alert">';
			html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
			html += data.message+'</div>';
			$("#ct_login_response").html(html);
		}
	});
}

function openMessage(msg_id) {
	$(".message-"+msg_id).slideToggle("slow");
	$("#caret-"+msg_id).toggleClass("down");
}

function showResponder(elem){
	var parent = $(elem).parent();
	parent.find(".btn-reponder").slideUp("slow");
	parent.find(".mail-response").slideDown("slow")
}
function cancelResponse(elem){
	var parent = $(elem).parent().parent().parent();
	parent.find(".btn-reponder").slideDown("slow");
	parent.find(".mail-response").slideUp("slow");
}

function displayMsgResponse(id, msg, type){
	if(!type) type = info;
	var html_msg = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + 
		'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
		msg + '</div>';
	$("#message-response-" + id).html(html_msg).slideDown("slow");
	return false;
}

function sendResponse(msg_id){
	var form = $("#form-response-" + msg_id);
	var msg = form.find("textarea").val();
	if(!msg) return displayMsgResponse(msg_id, "Mensagem vazia", "danger");
	var post_data = {
		origem: $("#origem").val(),
		destino: $("#destino-" + msg_id).val(),
		assunto: $("#assunto-" + msg_id).val(),
		answerto: msg_id,
		message: msg
	};
	$.ajax({
		url: "/Contato/Response", type: "POST", data: post_data,
		success: function(data){
			console.info(data);
			if(data.success) {
				window.setTimeout(function(){
					$(".response-box").slideUp("slow");
				}, 5000);
				displayMsgResponse(msg_id, data.message, "success");
			} else {
				displayMsgResponse(msg_id, data.message, "danger");
			}
		}
	});

}




