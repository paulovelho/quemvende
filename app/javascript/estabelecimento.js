$(document).ready(function(){
	$("#cadastrar-estabelecimento").validate({
		rules: {
			email: {
				email: true
			}
		},
		password2: {
			equalTo: "#password1"
		}
	});
});

function estabelecimentoEdit(){
	$("#estabelecimento_edit").slideUp("slow");
	$("#estabelecimento_info").slideDown("slow");
}
function estabelecimentoCloseEdit(){
	$("#estabelecimento_edit").slideDown("slow");
	$("#estabelecimento_info").slideUp("slow");
}
function estabelecimentoPassword(){
	$("#btn_changeSenha").slideUp("slow");
	$("#input_changeSenha").slideDown("slow");
}

function loadBuscasPage(page){
	$("#load-more-bt").remove();
	$.ajax("Estabelecimento/Buscas?page=" + page).success(function(data){
		$("#buscas-results").append(data);
	});
}

function loadContatosPage(page){
	$("#load-more-bt").remove();
	$.ajax("Estabelecimento/Contatos?page=" + page).success(function(data){
		$("#contatos-results").append(data);
	});
}

function EstabelecimentoAlterarSenha(){
	$("#message-response").hide();
	var hash = $("#hash").val();
	var id = $("#est-id").val();
	var newPassword1 = $("#new_password1").val();
	var newPassword2 = $("#new_password2").val();
	if(!newPassword1){
		$("#message-response").show().html("Preencha a senha!");
		return;
	}
	if(newPassword1 != newPassword2){
		$("#message-response").show().html("Senhas não conferem!");
		return;
	}
	var page = "/Estabelecimento/NovaSenha";
	$.ajax({
		url: page, type: "POST",
		data: {
			id: id,
			hash: hash,
			newPass: newPassword1
		},
		success: function(data){
			if(data.success == true){
				window.location.href = data.url;
			} else {
				$("#message-response").show().html(data.error);
			}
		}
	});
}