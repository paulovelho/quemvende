function searchHeader(){
	var terms = $("#search-header").val();
	if(!terms) return;
	var page = "/Produtos/Buscar/" + terms;
	window.location.href = page;
}

function loadProdutos(){
	var page = "/Produtos/GetProdutos";
	$.ajax(page).success(function(data){
		$("#list-produtos").html(data);
	});
}

function startProdutosDropzone(){
	var element = ".create-dropzone-file";
	var dpzone = new Dropzone(element, { 
		url: "/Produtos/addFile"
	}).on("success", function(file, result){
		console.info(result);
		if(result.success) {
			$("#new-produto").html("Processando arquivo enviado...");
			processFile(result.file_id);
		} else {
			$("#new-produto").html(result.error);
		}
	}).on("addedfile", function(){
		$(element+" .dz-image").html("<i class='fa fa-file'></i>");
		$(".dropzone-layer-file").fadeOut("slow");
		$("#format-sample").slideUp("slow");
	});
}

function processFile(fileId){
	var processUrl = "/Produtos/Process/?id="+fileId;
	$.ajax(processUrl).success(function(data){
		$("#format-sample").slideUp("slow", function(){
			$("#new-produto").html(data);
			$(this).colorbox.resize();
		});
	});
}

function addProdutos(){
	$.colorbox({ href: "/Produtos/Adicionar", "close": "<i class='fa fa-times-circle' alt='fechar' title='fechar'></i>" });
}

function adicionarProdutosManualmente(){
	$("#format-sample").fadeOut("slow");
	$("#new-produto").fadeOut("slow");
	$("#dropzone-produtos").fadeOut("slow", function(){
		$("#newProdutoForm").fadeIn("slow");
		$("#new-produto").html("");
	});
}

function addNewProduto(){
	var nome = $("#newprod-nome").val();
	var cat_id = $("#newprod-categoria").val();
	var cat_nome = $("#newprod-categoria option:selected ").text();
	var estilo = $("#newprod-estilo").val();
	var envase = $("#newprod-envase").val();

	$("#newprod-nome").val("");
	$("#newprod-estilo").val("");
	$("#newprod-envase").val("");

	var processUrl = "/Produtos/AddProduto";
	$.ajax({
		url: processUrl, type: "POST",
		data: { 
			nome: nome, 
			categoria: cat_id,
			estilo: estilo,
			envase: envase
		},
		success: function(data) {
			console.info(data);
			if(data.success){
				insertProdutoRow(nome, cat_nome, estilo, envase);
				loadProdutos();
			} else {
				$("#new-produto").html(data.message);
			}
			$("#new-produto").fadeIn("slow");
		}
	});
}
function insertProdutoRow(nome, categoria, estilo, envase){
	var html = '<div class="row">';
	html += '<div class="col-md-5">'+nome+'</div>';
	html += '<div class="col-md-2">'+categoria+'</div>';
	html += '<div class="col-md-2">'+estilo+'</div>';
	html += '<div class="col-md-2">'+envase+'</div>';
	html += '</div>';
	$("#new-produto").append(html);
}

function editProduto(prod_id){
	var nome = $("#prod-nome-"+prod_id).val();
	var cat_id = $("#prod-categoria-"+prod_id).val();
	var cat_nome = $("#prod-categoria-"+prod_id+" option:selected ").text();
	var estilo = $("#prod-estilo-"+prod_id).val();
	var envase = $("#prod-envase-"+prod_id).val();

	var processUrl = "/Produtos/EditProduto";
	var message = "";
	$.ajax({
		url: processUrl, type: "POST",
		data: { 
			id: prod_id,
			nome: nome, 
			categoria: cat_id,
			estilo: estilo,
			envase: envase
		},
		success: function(data) {
			if(data.success){
				$("#row-prod-"+prod_id+" .val-prod-nome").html(nome);
				$("#row-prod-"+prod_id+" .val-prod-categoria").html(cat_nome);
				$("#row-prod-"+prod_id+" .val-prod-estilo").html(estilo);
				$("#row-prod-"+prod_id+" .val-prod-envase").html(envase);
				message = data.message;
			} else {
				message = data.error;
			}
			cancelEditarProduto(prod_id);
			$("#prod-message-"+prod_id).html(message).slideDown("slow", function(){
				setTimeout(function(){ 
					setTimeout(function(){ $("#prod-message-"+prod_id).slideUp("slow"); }, 1000);
				}, 3000);
			});
		}
	});

}

function removeImportProduct(element){
	$(element).parent().remove();
}

function removeProduct(produto_id){
	var processUrl = "/Produtos/DeleteProduto/"+produto_id;
	$.ajax(processUrl).success(function(data){
		if(!data.success) {
			$("#prod-message-"+produto_id).html(data.error).slideDown("slow", function(){
				setTimeout(function(){ $("#prod-message-"+produto_id).slideUp("slow"); }, 3000);
			});
		} else {
			$("#prod-message-"+produto_id).html(data.message).slideDown("slow", function(){
				setTimeout(function(){ 
					$("#prod-message-"+produto_id).slideUp("slow");
					$("#row-prod-"+produto_id).slideUp("slow");
				}, 3000);
			});
		}
	});
}

function importProdutos(){
	var file_id = $("#fileid").val();
	var produtos = [];
	var prod = $(".importProduct");
	for(var i=0; i<prod.length; i++){
		var tr = prod[i];
		var p = {};
		p.categoria = $(tr).find("select[name='importCategoria']").val();
		p.nome = $(tr).find("input[name='importNome']").val();
		p.estilo = $(tr).find("input[name='importEstilo']").val();
		p.envase = $(tr).find("input[name='importEnvase']").val();
		produtos.push(p);
	};
	$.ajax({
		url: "/Produtos/ImportProducts", type: "POST",
		data: { produtos: produtos, file: file_id },
		success: function(data) {
			if(data.success){
				loadProdutos();
				$(this).colorbox.close();
			}
		}
	});
}
