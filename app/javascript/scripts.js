function Login(login_url, next_url){
	var post_data = {
		email: $(".loginbox #email").val(),
		password: $(".loginbox #password").val()
	};
	$("#login_loading").show("slow");
	if(!post_data.email || !post_data.password){
		$("#login_response").html("E-mail ou senha incorretos!");
		$("#login_loading").hide();
		return;
	}
	$.ajax({
		url: login_url, type: "POST", data: post_data,
		success: function(data){
			console.info(data);
			if( !data.success ){
				$("#login_loading").hide();
				$("#login_response").html(data.message);
			} else {
				window.location.href = next_url;
			}
		}
	});
}

function ChangePasswordGeral(){
	$("#login_response").html("");
	var page = "/Home/ChangeSenha";
	var id = $("#id").val();
	var key = $("#key").val();
	var type = $("#type").val();
	var password1 = $("#pass1").val();
	var password2 = $("#pass2").val();
	if(!password1) {
		$("#login_response").html("Preencha a senha!");
		return;
	}
	if(password1 != password2){
		$("#login_response").html("Senhas não conferem!");
		return;
	}
	$.ajax({
		url: page, type: "POST",
		data: {
			id: id, key: key, type: type,
			password: password1
		},
		success: function(data){
			console.info(data);
			if(data.success) {
				$(".loginbox").slideUp("slow");
				$("#main-message").html("<b>Senha alterada!</b>");
				setTimeout(function(){
					window.location.href = "/Home/Index";
				}, 3000);
			} else {
				$("#login_response").html(data.error);
			}
		}
	})
}

$(document).ready(function(){
	$('#search-header').keypress(function (e) {
		if (e.which == 13) {
			searchHeader();
		}
	});
});
