function toggleDistribuidoraEdit(show){
	if(show){
		$("#distribuidora-data-edit").slideUp("slow");
		$("#distribuidora-data").slideDown("slow");
	} else {
		$("#distribuidora-data-edit").slideToggle("slow");
		$("#distribuidora-data").slideToggle("slow");
	}
}

function toggleDistribuidoraAdvancedEdit(show){
	if(show){
		$("#distribuidora-data-adv-edit").slideUp("slow");
		$("#distribuidora-data-adv").slideDown("slow");
	} else {
		$("#distribuidora-data-adv-edit").slideToggle("slow");
		$("#distribuidora-data-adv").slideToggle("slow");
	}
}

function toggleDistribuidoraProdutosEdit(show){
	if(show){
		$("#distribuidora-produtos-edit").slideUp("slow");
		$("#distribuidora-produtos").slideDown("slow");
		loadProdutos();
	} else {
		$("#distribuidora-produtos-edit").slideToggle("slow");
		$("#distribuidora-produtos").slideToggle("slow");
	}
}

function togglePasswordChange(){
	$("#password-change-edit").slideToggle("slow");
	$("#password-change").slideToggle("slow");
}

function saveInfoBasic(element){
	var form = $("#dist-edit-basic");
	form.validate({
		rules: {
			email: {
				email: true
			}
		}
	});
	if(!form.valid()) return;
	$(element).find(".loading").show();
	var dataSend = {
		nome: $("#nome").val(),
		email: $("#email").val(),
		website: $("#website").val(),
		endereco: $("#endereco").val(),
		cidade: $("#cidade").val(),
		estado: $("#estado").val()
	};
	$.ajax({
		url: "/Distribuidora/SaveBasic", type: "POST",
		data: dataSend,
		success: function(data) {
			window.setTimeout(function(){
				console.info(data);
				$(".loading").hide();
				toggleDistribuidoraEdit(false);
				toggleDistribuidoraAdvancedEdit(true);
			}, 1000);
		}
	});
}
function saveInfoAdvanced(element){
	$(element).find(".loading").show();
	var dataSend = {
		slug: $("#slug").val(),
		areas: $("#areas").val(),
		condicoes: $("#condicoes").val()
	};
	$.ajax({
		url: "/Distribuidora/SaveAdvanced", type: "POST",
		data: dataSend,
		success: function(data) {
			console.info(data);
			if(data.success)
				$("#slug").val(data.slug);
			window.setTimeout(function(){
				$(".loading").hide();
				toggleDistribuidoraAdvancedEdit(false);
				toggleDistribuidoraProdutosEdit(true);
			}, 2000);
		}
	});
}

function changePassword(){
	var oldPassword = $("#actual_password").val();
	var newPassword1 = $("#new_password1").val();
	var newPassword2 = $("#new_password2").val();
	if(!oldPassword || !newPassword1){
		$("#password-response").show().html("Preencha todos os campos!");
		return;
	}
	if(newPassword1 != newPassword2){
		$("#password-response").show().html("Senhas não conferem!");
		return;
	}
	$("#password-response").show().html("Carregando...");
	var page = "/Distribuidora/TrocarSenha";
	$.ajax({
		url: page, type: "POST",
		data: {
			oldPass: oldPassword,
			newPass: newPassword1
		},
		success: function(data){
			if(data.success == true){
				$("#password-response").show().html("Senhas alteradas com sucesso!");
				window.setTimeout(function(){
					togglePasswordChange();
					$("#password-response").hide();
				}, 2000);
			} else {
				$("#password-response").show().html(data.message);
			}
		}
	});
}

function ConcluirCadastro(){
	$("#message-response").hide();
	var hash = $("#hash").val();
	var id = $("#dist-id").val();
	var nome = $("#nome").val();
	var email = $("#email").val();
	var newPassword1 = $("#new_password1").val();
	var newPassword2 = $("#new_password2").val();
	if(!nome || !email || !newPassword1){
		$("#message-response").show().html("Preencha todos os campos!");
		return;
	}
	if(newPassword1 != newPassword2){
		$("#message-response").show().html("Senhas não conferem!");
		return;
	}
	var page = "/Distribuidora/CompletarCadastro";
	$.ajax({
		url: page, type: "POST",
		data: {
			id: id,
			hash: hash,
			nome: nome,
			email: email,
			newPass: newPassword1
		},
		success: function(data){
			if(data.success == true){
				window.location.href = data.url;
			} else {
				$("#message-response").show().html(data.error);
			}
		}
	});
}

function CadastrarDistribuidora(){
	var dataSend = {
		nome: $("#nome").val(),
		email: $("#email").val(),
		website: $("#website").val(),
		endereco: $("#endereco").val(),
		cidade: $("#cidade").val(),
		estado: $("#estado").val()
	};
	$.ajax({
		url: "/Distribuidora/Cadastrar", type: "POST",
		data: dataSend,
		success: function(data) {
			if(data.error) {
				$("#response").html(data.message).slideDown("slow");
			}
		}
	});
}

function checkSlug(){
	var slug = $("#slug").val();
	$.ajax("/Distribuidora/CheckSlug/"+slug).success(function(data){
		if(!data) {
			$("#valid-slug").removeClass("red").addClass("green").html("<i class='fa fa-check'></i>")
		} else {
			var id = $("#dist-id").val();
			if(data.id != id)
				$("#valid-slug").removeClass("green").addClass("red").html("<i class='fa fa-times'></i> URL não disponível")
			else
				$("#valid-slug").removeClass("green").removeClass("red").html("");
		}
	});
}

function loadDropLogo(){
	$("#dist-logo").slideUp("slow");
	$("#add-logo").slideDown("slow");
	$(".btn-change-logo").hide("slow");
	$(".btn-cancel-logo").show("slow");
	if(!$(".create-dropzone-logo").hasClass("dz-clickable")){
		var droplogo = addDropzone(".create-dropzone-logo");
		droplogo.on("success", function( file, result ) {
			console.info(result);
			if(!result.image) {
				$("#dist-logo").html("Erro adicionando imagem!");
				cancelDropLogo();
				return;
			}
			$.ajax({
				url: "/Distribuidora/SetImage", type: "POST",
				data: { image_id: result.image.id },
				success: function(data){
//					console.info(data);
					$.ajax({
						url: "/Distribuidora/Logo",
						success: function(data){
							if(data.img == false){
								$("#dist-logo").html("Erro: " + data.error);
							} else {
								$("#dist-logo").html(data);
							}
							cancelDropLogo();
						}
					})
				}
			})
		});
		droplogo.on("addedfile", function(){
			$(".dropzone-layer-logo").fadeOut("slow");
		});
	} else {
		// otherwise, dropzone is already attached... ;)
		$(".create-dropzone-logo").html("");
		$(".dropzone-layer-logo").show();
	}
}
function cancelDropLogo(){
	$("#dist-logo").slideDown("slow");
	$("#add-logo").slideUp("slow");
	$(".btn-change-logo").show("slow");
	$(".btn-cancel-logo").hide("slow");
}



