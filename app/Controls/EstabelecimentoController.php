<?php

class EstabelecimentoController extends MagratheaController {

	public function Index(){
		$est = unserialize(@$_SESSION["estabelecimento"]);
		if($est)
			$this->Home();
		else
			$this->Smarty->display("quemvende/estabelecimento_index.html");
	}

	public function Home(){
		$est = unserialize($_SESSION["estabelecimento"]);
		$this->Smarty->assign("estabelecimento", $est);
		$estados = EstadoControl::GetEstados();
		$this->Smarty->assign("estados", $estados);
		$this->Smarty->display("quemvende/estabelecimento_home.html");
	}

	public function Buscas(){
		$est = unserialize($_SESSION["estabelecimento"]);
		$page = @$_GET["page"];
		if(empty($page)) $page = 0;
		$perPage = 10;
		$buscas = BuscaControl::GetByEstabelecimento($est->id, $page, $perPage);
		$this->Smarty->assign("page", $page);
		$this->Smarty->assign("buscas", $buscas);
		$this->Smarty->assign("qtd", count($buscas));
		$this->Smarty->display("quemvende/estabelecimento_buscas.html");
	}

	public function Contatos(){
		$est = unserialize($_SESSION["estabelecimento"]);
		$page = @$_GET["page"];
		if(empty($page)) $page = 0;
		$perPage = 10;
		$contatos = ContatoControl::GetByEstabelecimento($est->id, $page, $perPage);
		$this->Smarty->assign("page", $page);
		$this->Smarty->assign("contatos", $contatos);
		$this->Smarty->assign("qtd", count($contatos));
		$this->Smarty->display("quemvende/estabelecimento_contatos.html");
	}

	public function Cadastro(){
		$estados = EstadoControl::GetEstados();
		$this->Smarty->assign("estados", $estados);
		$this->Smarty->display("quemvende/estabelecimento_cadastro.html");
	}

	public function Editar(){
		$estados = EstadoControl::GetEstados();
		$this->Smarty->assign("estados", $estados);
		$this->Smarty->display("quemvende/estabelecimento_form.html");
	}

	public function CadastroRapido(){
		$email = $_POST["email"];
		$est = new Estabelecimento();
		if(EstabelecimentoControl::EmailExists($email)){
			$this->Json(array("success" => false, "message" => "Senhas não conferem!"));
		}
		$est->email = $email;
		$est->SetPassword($_POST["pass"]);
		$est->contato = $_POST["nome"];
		$est->nome = $_POST["estab"];
		$est->estado_id = $_POST["estado"];
		$est->last_login = now();
		if($est->Save()){
			$_SESSION["estabelecimento"] = serialize($est);
			BuscaControl::SetEstabelecimento($est->id);
			$this->Json(array("success" => true));
		} else {
			$this->Json(array("success" => false, "message" => "Erro ao salvar cadastro!"));
		}
	}

	public function Welcome($params){
		$params = explode("/", $params);
		$id = $params[0];
		$hash = $params[1];

		$est = new Estabelecimento($id);
		$estHash = md5($est->password."reset420");
		if($estHash == $hash){
			$this->Smarty->assign("hash", $hash);
			$this->Smarty->assign("est", $est);
			$this->Smarty->display("quemvende/estabelecimento_new.html");
		} else {
			$this->Smarty->assign("title", "Erro ao acessar estabelecimento!");
			$message = "Houve algum problema no seu link de acesso.<br/>";
			$message .= "Para mais informações, <a href='/Contato'>entre em contato</a>.";
			$this->Smarty->assign("message", $message);
			$this->Smarty->display("quemvende/message.html");
		}
	}
	public function NovaSenha(){
		$est = new Estabelecimento($_POST["id"]);
		$estHash = md5($est->password."reset420");
		if($estHash == $_POST["hash"]){
			$est->SetPassword($_POST["newPass"]);
			$est->Save();
			$_SESSION["estabelecimento"] = serialize($est);
			return $this->Json(
				array('success' => true, 'url' => "/Estabelecimento/Home")
			);
		} else {
			return $this->Json(
				array('success' => false, 'error' => "Ocorreu um erro inesperado!")
			);
		}
	}

	public function Salvar(){
		$id = @$_POST["id"];
		$email = $_POST["email"];
		$est = new Estabelecimento($id);
		if($email != $est->email){
			if(EstabelecimentoControl::EmailExists($email)){
				$this->Smarty->assign("title", "Cadastro de estabelecimento");
				$this->Smarty->assign("message", "Houve um erro no seu cadastro:<br/><br/>Este e-mail já está em uso!");
				$this->Smarty->display("quemvende/message.html");
				return;
			}
		}
		if(empty($id)) {
			$est->SetPassword($_POST["password1"]);
		} else {
			$pass = @$_POST["password"];
			$newPass = @$_POST["password1"];
			if(!empty($pass) && !empty($newPass)){
				if($est->password == md5($pass)){
					if($_POST["password1"] != $_POST["password2"]){
						$this->Smarty->assign("alertStyle", "danger");
						$this->Smarty->assign("message", "Senhas não conferem!");
						$this->Home();
						die;
					} else {
						$est->SetPassword($newPass);
					}
				} else {
					$this->Smarty->assign("alertStyle", "danger");
					$this->Smarty->assign("message", "Senha incorreta!");
					$this->Home();
					die;
				}
			}
		}
		$est->email = $email;
		$est->contato = $_POST["nome"];
		$est->nome = $_POST["estabelecimento"];
		$est->endereco = $_POST["endereco"];
		$est->cidade = $_POST["cidade"];
		$est->estado_id = $_POST["estado"];
		$est->last_login = now();
		if($est->Save()){
			$_SESSION["estabelecimento"] = serialize($est);
			BuscaControl::SetEstabelecimento($est->id);
			if(empty($id)){
				$this->Smarty->assign("alertStyle", "info");
				$this->Smarty->assign("message", "Bem vindo ao <span class='destaque'>QuemVende?</span>!");
			} else {
				$this->Smarty->assign("alertStyle", "success");
				$this->Smarty->assign("message", "Dados alterados com sucesso!");
			}
			$this->Home();
		} else {
			$this->Smarty->assign("title", "Cadastro de estabelecimento");
			$this->Smarty->assign("message", "Houve um erro no seu cadastro!");
			$this->Smarty->display("quemvende/message.html");
			return;
		}
	}

	public function Login(){
		$est = EstabelecimentoControl::Login($_POST["email"], $_POST["password"]);
		if(!empty($est)){
			$_SESSION["estabelecimento"] = serialize($est);
			BuscaControl::SetEstabelecimento($est->id);
			$this->Json(array("success" => true, "estabelecimento" => $est));
		} else {
			$ret = "Usuário não encontrado ou senha incorreta. <br/>";
			$ret .= "Caso não esteja cadastrado, <a href='/Distribuidora/Cadastro'>clique aqui</a> para efetuar o cadastro.<br/>";
			$ret .= "Para maiores informações, <a href='/Contato'>entre em contato</a>";
			$this->Json(
				array("success" => false, "message" => $ret)
			);
		}
	}


}

?>