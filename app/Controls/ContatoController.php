<?php

class ContatoController extends MagratheaController {

	public function Index(){
		$this->display("quemvende/contato.html");
	}

	public function Send(){
		$mensagem = "Nova mensagem de [QUEMVENDE]: <br/>";
		$mensagem .= "origem: ['".$_POST["nome"]."' <".$_POST["email"].">]<br/>";
		$mensagem .= "---<br/><br/>".$_POST["mensagem"];

		$contato = new Contato();
		$contato->email_from = $_POST["email"];
		$contato->email_to = "contato@quemven.de";
		$contato->assunto = $_POST["assunto"];
		$contato->mensagem = $mensagem;
		$contato->Send();

		$msg = "Muito obrigado pela sua mensagem!<br/><br/>";
		$msg .= "Retornaremos o contato tão logo quanto possível!";

		$this->assign("title", "Mensagem enviada");
		$this->assign("message", $msg);
		$this->display("quemvende/message.html");
	}

	public function Interaction(){
		$distribuidora = new Distribuidora($_POST["dist_id"]);

		$mensagem = "contato de: ".$_POST["responsavel"].", ".$_POST["estabelecimento"]." [".$_POST["email"]."]<br/><br/>";
		$mensagem .= "<b>".$_POST["assunto"]."</b><br/>".$_POST["message"];

		$contato = new Contato();
		$contato->email_from = $_POST["email"];
		$contato->email_to = $distribuidora->email;
		$contato->assunto = $_POST["assunto"];
		$contato->origem = "estabelecimento";
		$contato->mensagem = $mensagem;
		$contato->SetDistribuidora($distribuidora);
		$contato->SetEstabelecimento($estabelecimento);
		if($contato->Send()){
			$this->Json(array("success" => true, "message" => "Contato enviado com sucesso!"));
		} else {
			$this->Json(array("success" => false, "message" => "Erro ao estabelecer contato!"));
		}
	}

	public function Response(){
		$mensagem = "";
		$email_from = "";
		$email_to = "";
		$answer = @($_POST["answerto"] ? $_POST["answerto"] : 0);

		if($_POST["origem"] == "estabelecimento") {
			$estabelecimento = unserialize($_SESSION["estabelecimento"]);
			if(empty($estabelecimento->id)) $this->Json(array("success" => false, "message" => "Sessão inválida!"));
			$distribuidora = new Distribuidora($_POST["destino"]);

			$email_from = $estabelecimento->email;
			$email_to = $distribuidora->email;
			$mensagem = "contato de: ".$estabelecimento->contato.", ".$estabelecimento->nome." [".$email_from."]<br/><br/>";

		} else if($_POST["origem"] == "distribuidora") {
			$distribuidora = unserialize($_SESSION["distribuidora"]);
			if(empty($distribuidora->id)) $this->Json(array("success" => false, "message" => "Sessão inválida!"));
			$estabelecimento = new Estabelecimento($_POST["destino"]);

			$email_from = $distribuidora->email;
			$email_to = $estabelecimento->email;
			$mensagem = "contato de: ".$distribuidora->nome." [".$email_from."]<br/><br/>";
		} else {
			$this->Json(array("error" => "Argumentos inválidos"));
		}

		$mensagem .= "<b>".$_POST["assunto"]."</b><br/>".$_POST["message"];

		$contato = new Contato();
		$contato->email_from = $email_from;
		$contato->email_to = $email_to;
		$contato->assunto = $_POST["assunto"];
		$contato->origem = $_POST["origem"];
		$contato->answer_to = $answer;
		$contato->mensagem = $mensagem;
		$contato->SetDistribuidora($distribuidora);
		$contato->SetEstabelecimento($estabelecimento);
		if($contato->Send()){
			$this->Json(array("success" => true, "message" => "Contato enviado com sucesso!", "contato" => $contato));
		} else {
			$this->Json(array("success" => false, "message" => "Erro ao estabelecer contato!"));
		}

	}

}

?>