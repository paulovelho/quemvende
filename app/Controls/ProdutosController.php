<?php

class ProdutosController extends MagratheaController {

	public function Buscar($query) {

		$page = @$_GET["page"];
		if(empty($page)) $page = 0;
		else $page = $page-1;
		$perPage = 10;
		$result = ProdutoControl::Search($query, $total, $page, $perPage);
//		p_r($result);
		$totalPages = ceil($total/$perPage);

		$b = new Busca();
		$b->Q($query);

		if($page == 0) {
			$distribuidoras = DistribuidoraControl::Search($query);
			if(count($distribuidoras) > 0)
				$this->assign("distribuidoras", $distribuidoras);
		}

		$estados = EstadoControl::GetEstadosArr();
		$this->Smarty->assign("estados", $estados);

		$this->assign("total", $total);
		$this->assign("page", ($page+1));
		$this->assign("totalPages", $totalPages);
		$this->assign("q", $query);
		$this->assign("busca", $result);
		$this->display("quemvende/busca.html");
	}

	public function Adicionar(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->ForwardTo("Home", "Index");
		$categorias = CategoriaControl::GetActive();
		$this->assign("categorias", $categorias);
		$this->display("quemvende/produtos_add.html");
	}

	public function GetProdutos($dist_id){
		if(empty($dist_id)){
			$dist = @unserialize($_SESSION["distribuidora"]);
			if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));
			$dist_id = $dist->id;
		}
		$produtos = ProdutoControl::GetByDistribuidora($dist_id);
//		p_r($produtos);
		$categorias = CategoriaControl::GetActive();
		$this->assign("categorias", $categorias);
		$this->assign("produtos", $produtos);
		$this->display("quemvende/produtos_list_edit.html");
	}

	public function AddProduto(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));

		$prod = new Produto();
		$prod->distribuidora_id = $dist->id;
		$prod->SetNome($_POST["nome"]);
		$prod->SetEstilo($_POST["estilo"]);
		$prod->envase = $_POST["envase"];
		$prod->categoria_id = $_POST["categoria"];

		if($prod->Insert()){
			$this->Json(array('success' => true, 'message' => "Produto adicionado!"));
		}
	}

	public function EditProduto(){
		$prod_id = $_POST["id"];

		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));
		
		$prod = new Produto($prod_id);
		if(empty($prod->id)) $this->Json(array('success' => false, 'error' => "Produto inexistente"));
		if($prod->distribuidora_id != $dist->id) $this->Json(array('success' => false, 'error' => "Operação inválida"));

		$prod->SetNome($_POST["nome"]);
		$prod->SetEstilo($_POST["estilo"]);
		$prod->envase = $_POST["envase"];
		$prod->categoria_id = $_POST["categoria"];

		if($prod->Save()){
			$this->Json(array('success' => true, 'message' => "Alterações Salvas!"));
		}
	}

	public function DeleteProduto($prod_id){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));
		
		$prod = new Produto($prod_id);
		if(empty($prod->id)) $this->Json(array('success' => false, 'error' => "Produto inexistente"));
		if($prod->distribuidora_id != $dist->id) $this->Json(array('success' => false, 'error' => "Operação inválida"));

		$prod->Delete();
		$this->Json(array('success' => true, 'message' => "Produto apagado!"));

	}

	public function addFile(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));

		$filename = $dist->id."_".$dist->slug."-".date("Ymdhis")."-".$_FILES["file"]["name"];
		$folder = MagratheaConfig::Instance()->GetFromDefault("upfiles_path");
		if(!is_writeable($folder)){
			$this->Json(array('success' => false, 'error' => 'Folder does not exists or does not have permissions: ['.$folder.']'));
		}
		$filefinal = $folder."/".$filename;
		move_uploaded_file($_FILES["file"]["tmp_name"], $filefinal);

		if(!file_exists($filefinal)){
			$this->Json(array('success' => false, 'error' => 'Erro ao subir arquivo!'));
		}
		$arquivo = new Arquivo();
		$arquivo->distribuidora_id = $dist->id;
		$arquivo->nome = $filename;
		$arquivo->Insert();
		$this->Json(array('success' => true, 'file_id' => $arquivo->id));
	}

	public function ImportProducts(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));

		$prods = $_POST["produtos"];
		$file_id = $_POST["file"];
		$saved = 0;
		foreach ($prods as $p) {
			$produto = new Produto();
			$produto->distribuidora_id = $dist->id;
			$produto->arquivo_id = $file_id;
			$produto->SetNome($p["nome"]);
			$produto->categoria_id = $p["categoria"];
			$produto->SetEstilo($p["estilo"]);
			$produto->envase = $p["envase"];
			$produto->Insert();
			$saved ++;
		}
		$this->Json(array('success' => true, 'qtd' => $saved));
	}

	public function Process(){
		$file_id = @$_GET["id"];
//		$isFormatted = @$_GET["formatted"];
//		$format = @$_GET["format"];

		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist->id)) $this->Json(array('success' => false, 'error' => 'Erro na sessão!'));
		if(empty($file_id)) $this->Json(array('success' => false, 'error' => 'Erro ao processar arquivo!'));

		$arquivo = new Arquivo($file_id);
		$folder = MagratheaConfig::Instance()->GetFromDefault("upfiles_path");
		$lines = file($folder."/".$arquivo->nome, FILE_IGNORE_NEW_LINES);
		$produtos = array();

		$firstLine = $lines[0];
		$explodedHeader = explode(";", $firstLine);
		$format = null; $formatted = false;
		if(count($explodedHeader) == 4){
			foreach($explodedHeader as $h){
				if($h == "nome" || $h =="name"){
					$format = array_map("trim", $explodedHeader);
					array_shift($lines);
				}
			}
			$formatted = true;
		}

		foreach ($lines as $l) {
			if($formatted){
				$arr = $this->ReturnProductsFormatted($l, $format);
				$p = new Produto();
				$p->categoria_id = $this->SwitchCategoria($arr["categoria"]);
				$p->SetEstilo($arr["estilo"]);
				$p->envase = $arr["envase"];
				$p->SetNome($arr["nome"]);
			} else {
				$p = $this->CrawlProduct($l);
			}
			array_push($produtos, $p);
		}
		$this->assign("produtos", $produtos);
		$categorias = CategoriaControl::GetActive();
		$this->assign("categorias", $categorias);
		$this->assign("file", $file_id);
		$this->display("quemvende/produtos_import.html");
	}

	private function ReturnProductsFormatted($p, $format=null){
		$prod = array();
		if(empty($format)){
			$format = array('categoria','nome','estilo','envase');
		}
		$spl = explode(";", $p);
		for($i = 0; $i < count($spl); $i++){
			$prod[$format[$i]] = trim($spl[$i]);
		}
		return $prod;
	}

	private function SwitchCategoria($c){
		$limpador = new ClearString();
		$s_clean = $limpador->clear_string($c);
		switch ($s_clean) {
			case 'cerveja':
				return 1;
				break;
			case 'cacaca':
				return 2;
				break;
			case 'vino':
				return 3;
				break;
			case 'destilado':
				return 4;
				break;
			case 'agua':
				return 5;
				break;
			case 'refrigerante':
				return 6;
				break;
			case 'suco':
				return 7;
				break;
			case 'espumante':
				return 8;
				break;
			default:
				return 9;
				break;
		}
	}

	private function CrawlProduct($s){
		$produto = new Produto();

		$limpador = new ClearString();
		$s_clean = $limpador->clear_string($s);
//		echo $s_clean."<br/>";

		$pattern = "/\b-teste-\b/i";
		if (preg_match('/\bcerveja[\w]?|\bbeer\b|\bbier\b/i', $s)) {
			// cerveja = 1
			$produto->categoria_id = 1;
			// trocar nome da categoria:
			$cerveja = '/\bcerveja[\w]?/i';
			$s = preg_replace($cerveja, "", $s);
			$produto->SetEstilo($this->SearchBeerStyle($s));
		} else if(preg_match("/\bcacaca[\w]?/i", $s_clean)) {
			// cachaça = 2
			$produto->categoria_id = 2;
		} else if(preg_match("/\bvino[\w]?|\bcinta[\w]?/i", $s_clean)) {
			// vinho = 3
			$produto->categoria_id = 3;
			$produto->SetEstilo($this->SearchWineStyle($s));
		} else if(preg_match("/\bvodka[\w]?|\bbourbon\b|\buis\w/i", $s_clean)) {
			// destilado = 4
			$produto->categoria_id = 4;
		} else if(preg_match("/\bagua[\w]?/i", $s_clean)) {
			// água = 5
			$produto->categoria_id = 5;
		} else if(preg_match("/\bcola\b|\brefrigerante\b|\bguarana\b/i", $s_clean)) {
			// refrigerante = 6
			$produto->categoria_id = 6;
		} else if(preg_match("/\bsuco[\w]?/i", $s_clean)) {
			// suco = 7
			$produto->categoria_id = 7;
		} else if(preg_match("/\bcampane[\w]?|\bespumante[\w]?|\bcidra[\w]?/i", $s_clean)) {
			// espumante = 8
			$produto->categoria_id = 8;
		} else {
			$produto->categoria_id = 9;
			$estilo = $this->SearchBeerStyle($s);
			if(!empty($estilo)){
				$produto->SetEstilo($estilo);
				$produto->categoria_id = 1;
			} else {
				$estilo = $this->SearchWineStyle($s);
				if(!empty($estilo)){
					$produto->categoria_id = 3;
					$produto->SetEstilo($estilo);
				}
			}
		}

		$produto->envase = $this->SearchEnvase($s);
		$s = str_replace(";", "", $s);
		$s = trim($s);
		$produto->SetNome($s);
		return $produto;
	}

	private function SearchEnvase(&$s){
		$envase = "";
		$envaseSize = "";

		// look for size:
		$pattern = '/[^;\s]+[ ]*ml\b|[\d]+[ ]*l[\w+]*/i';
		if(preg_match($pattern, $s, $match)){
			$envaseSize = str_replace(" ", "", $match[0]);
			$s = str_replace($match[0], "", $s);
		}
		// look for container:
		$pattern = '/\blat[\S]*|\bgarraf[\S]*|\blong neck\b|\bgal[\S]*|\bbarril\b|\bpet\b/i';
		if(preg_match($pattern, $s, $match)){
			$envase = str_replace(" ", "", $match[0]);
			$s = str_replace($match[0], "", $s);
		}

		$envase .= " ".$envaseSize;
		return trim($envase);
	}

	private function SearchBeerStyle($s){
		// weiss:
		$pattern = '/\wei\S*\b/i';
		if(preg_match($pattern, $s)) return "Weiss";
		// pilsen:
		$pattern = '/\bpilsen\b/i';
		if(preg_match($pattern, $s)) return "Pilsen";
		// ipa:
		$pattern = '/\bipa\b|\bindia[\s\S]*ale\b/i';
		if(preg_match($pattern, $s)) return "IPA";
		// stout:
		$pattern = '/\bstout\b/i';
		if(preg_match($pattern, $s)) return "Stout";
		// ale:
		$pattern = '/\bale\b/i';
		if(preg_match($pattern, $s)) return "Ale";

		return null;
	}

	private function SearchWineStyle($s){
		$pattern = '/pino\S*\b|\bcab[\s\S]*on\b|\bsau[\s\S]*on\b|\bmerlot\b|\bmalbec\b/i';
		if(preg_match($pattern, $s, $match)){
			return $match[0];
		}
		return null;
	}

}

?>