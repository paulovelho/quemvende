<?php

class HomeController extends MagratheaController {

	public function Index(){
		$this->Smarty->display("quemvende/index.html");
	}

	public function Logout(){
		session_destroy();
		return $this->ForwardTo("Home", "Index");
	}

	public function EsqueciASenha(){
		$this->assign("email", "");
		$this->display("quemvende/esqueci_a_senha.html");
	}

	public function SendSenha(){
		$email = $_POST["email"];
		$this->assign("email", $email);
		$dest = DistribuidoraControl::GetByEmail($email);
		if(!@$dest->id){
			$dest = EstabelecimentoControl::GetByEmail($email);
		}
		if($dest) {
			if(ContatoControl::SendLembrarSenha($dest)){
				$message = "success";
			} else {
				$message = "error";
			}
		} else {
			$message = "notfound";
		}
		$this->assign("message", $message);
		$this->display("quemvende/esqueci_a_senha.html");
	}

	public function NovaSenha(){
		
		$dist_id = @$_GET["dist_id"];
		$estab_id = @$_GET["estab_id"];
		$hash_key = @$_GET["key"];

		$message = "";
		$showForm = false;
		$type = "";
		$id = 0;
		if(!empty($dist_id)){
			try {
				$dist = new Distribuidora($dist_id);
				$password = substr( md5($dist->password), 0, 10);
				if($password == $hash_key){
					$message .= "<h4>Distribuidora: ".$dist->nome."</h4>";
					$message .= "Insira uma nova senha para o usuário [".$dist->email."]<br/>";
					$showForm = true;
					$id = $dist_id;
					$type = "distribuidora";
				} else {
					$message .= "Erro na validação de hash!<br/>";
				}

			} catch(Exception $ex){
				$message .= "Erro nas informações de distribuidora!<br/>";
			}
		} else if(!empty($estab_id)){
			try {
				$dist = new Distribuidora($dist_id);
			} catch(Exception $ex){
				$message .= "Erro nas informações de estabelecimento!<br/>";
			}
		} else {
			$this->ForwardTo("Home", "Index");
			die;
		}

		if(!$showForm)
			$message .= "<br/>Para mais informações ou dúvidas, <a href='/Contato'>entre em contato</a>.";

		$this->assign("message", $message);
		$this->assign("showForm", $showForm);
		$this->assign("id", $id);
		$this->assign("key", $hash_key);
		$this->assign("type", $type);
		$this->display("quemvende/redefinir_senha.html");
	}

	public function ChangeSenha(){
		$id = @$_POST["id"];
		$key = @$_POST["key"];
		$type = @$_POST["type"];
		$password = @$_POST["password"];

		try{ 
			if($type == "distribuidora") {
				$dist = new Distribuidora($id);
				$actualkey = substr( md5($dist->password), 0, 10);
				if($actualkey != $key){
					$this->Json(array("success" => false, "error" => "Erro na validação!"));
				} else {
					$dist->SetPassword($password);
					$dist->Save();
					$this->Json(array("success" => true));
				}
			}
		} catch(Exception $ex) {
			$this->Json(array("success" => false, "error" => $ex->getMessage()));
		}
	}

	public function Privacidade(){
		$this->display("quemvende/privacidade.html");
	}

	public function Termos(){
		$this->display("quemvende/termos.html");
	}

}

?>