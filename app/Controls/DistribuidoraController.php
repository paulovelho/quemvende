<?php

class DistribuidoraController extends MagratheaController {

	public function NotFound(){
		$this->Smarty->display("quemvende/distribuidora_notfound.html");
	}

	public function Index(){
		$this->Smarty->display("quemvende/distribuidora_index.html");
	}

	public function Contato(){
		$est = @unserialize($_SESSION["estabelecimento"]);
		if(@$est->id) $this->Smarty->assign("est", $est);
		$estados = EstadoControl::GetEstados();
		$this->Smarty->assign("estados", $estados);
		$this->Smarty->display("quemvende/distribuidora_contato.html");
	}

	public function Contatos(){
		$dist = unserialize($_SESSION["distribuidora"]);
		if($dist == null) $this->Json(false);
		$this->Smarty->display("quemvende/distribuidora_contatos.html");
	}

	public function ListContatos(){
		$dist = unserialize($_SESSION["distribuidora"]);
		if($dist == null) $this->Json(false);
		$page = @$_GET["page"];
		if(empty($page)) $page = 0;
		$perPage = 100;
		$contatos = ContatoControl::GetByDistribuidora($dist->id, $page, $perPage);
		$this->Smarty->assign("page", $page);
		$this->Smarty->assign("contatos", $contatos);
		$this->Smarty->assign("qtd", count($contatos));
		$this->Smarty->display("quemvende/distribuidora_contatos_list.html");
	}

	public function Cadastro(){
		$estados = EstadoControl::GetEstados();
		$this->Smarty->assign("estados", $estados);
		$this->Smarty->display("quemvende/distribuidora_cadastro.html");
	}
	public function Cadastrar(){
		$dist = new Distribuidora();
		$dist->SetNome($_POST["nome"]);
		$dist->email = $_POST["email"];
		$dist->SetWebsite($_POST["website"]);
		$dist->endereco = $_POST["endereco"];
		$dist->cidade = $_POST["cidade"];
		$dist->estado_id = $_POST["estado"];
		$dist->password = "quemvende";
		$dist->active = false;
		$dist->views = 0;
		$dist->Save();

		$mensagem = "nova distribuidora cadastrada<br/>";
		$mensagem .= "Nome: ".$dist->nome."<br/>";
		$mensagem .= "E-mail: ".$dist->email."<br/>";
		$mensagem .= "Website: <a href='".$dist->website."'>".$dist->website."</a><br/>";
		$mensagem .= "Endereco: ".$dist->endereco."<br/>";
		$mensagem .= "Cidade: ".$dist->cidade."<br/>";

		$contato = new Contato();
		$contato->email_to = "quemvende.cadastro@paulovelho.com";
		$contato->email_from = $dist->email;
		$contato->assunto = "[quemvende] (nova distribuidora) [".$dist->nome."]";
		$contato->mensagem = $mensagem;
		$contato->Send();

		$msg = "Obrigado pelo seu cadastro!<br/><br/>";
		$msg .= "Após uma breve etapa de aprovação, você receberá um link de confirmação de cadastro da sua empresa.<br/>";
		$msg .= "Basta seguir as instruções e adicionar seus produtos para que eles fiquem disponíveis na base de dados!<br/><br/><br/>";
		$msg .= "Obrigado pela participação e esperamos que este serviço possa lhe render bons negócios!";
		$this->Smarty->assign("title", "Cadastro de disitribuidora");
		$this->Smarty->assign("message", $msg);
		$this->Smarty->display("quemvende/message.html");
	}

	public function Welcome($params){
		$params = explode("/", $params);
		$id = explode("-", $params[0])[1] - 420;
		$hash = $params[1];

		$dist = new Distribuidora($id);
		$distHash = md5($dist->password."reset420");
		if($distHash == $hash){
			$this->Smarty->assign("hash", $hash);
			$this->Smarty->assign("dist", $dist);
			$this->Smarty->display("quemvende/distribuidora_new.html");
		} else {
			$this->Smarty->assign("title", "Erro ao acessar distribuidora!");
			$message = "Houve algum problema no seu link de acesso.<br/>";
			$message .= "Para mais informações, <a href='/Contato'>entre em contato</a>.";
			$this->Smarty->assign("message", $message);
			$this->Smarty->display("quemvende/message.html");
		}
	}
	public function CompletarCadastro(){
		$dist = new Distribuidora($_POST["id"]);
		$distHash = md5($dist->password."reset420");
		if($distHash == $_POST["hash"]){
			$dist->SetPassword($_POST["newPass"]);
			$dist->SetNome($_POST["nome"]);
			$dist->email = $_POST["email"];
			$dist->Save();
			$_SESSION["distribuidora"] = serialize($dist);
			return $this->Json(
				array('success' => true, 'url' => "/Distribuidora/Editar")
			);
		} else {
			return $this->Json(
				array('success' => false, 'error' => "Ocorreu um erro inesperado!")
			);
		}
	}

	public function Editar(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist))
			return $this->Redirect("Home", "Index");
		else {
			$this->Smarty->assign("dist", $dist);
			$estados = EstadoControl::GetEstados();
			$this->Smarty->assign("estados", $estados);
			$this->Smarty->display("quemvende/distribuidora_edit.html");
		}
	}
	public function SaveBasic(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist)) die;
		$dist->SetNome($_POST["nome"]);
		$dist->email = $_POST["email"];
		$dist->SetWebsite($_POST["website"]);
		$dist->endereco = $_POST["endereco"];
		$dist->cidade = $_POST["cidade"];
		$dist->estado_id = $_POST["estado"];
		$dist->Save();
		$_SESSION["distribuidora"] = serialize($dist);
		$this->Json(true);
	}
	public function SaveAdvanced(){
		$dist = @unserialize($_SESSION["distribuidora"]);
		if(empty($dist)) die;
		if($_POST["slug"] && $dist->slug != $_POST["slug"]){
			$distSlug = DistribuidoraControl::GetBySlug($_POST["slug"]);
			if($distSlug == null)
				$dist->SetSlug($_POST["slug"]);
		}
		$dist->areas = $_POST["areas"];
		$dist->condicoes = $_POST["condicoes"];
		$dist->Save();
		$_SESSION["distribuidora"] = serialize($dist);
		$this->Json(array("success" => true, "slug" => $dist->slug));
	}

	public function TrocarSenha(){
		$dist = unserialize($_SESSION["distribuidora"]);
		$oldPass = $_POST["oldPass"];
		$newPass = $_POST["newPass"];
		if(md5($oldPass) != $dist->password){
			$this->Json(
				array('success' => false, 'message' => "A senha atual está incorreta!")
			);
		} else {
			$dist->SetPassword($newPass);
			$dist->Save();
			$this->Json(
				array('success' => true)
			);
		}
	}

	public function Logo(){
		$slug = @$_GET["d"];
		if(!empty($slug)){
			$dist = DistribuidoraControl::GetBySlug($slug);
		} else {
			$dist = @unserialize($_SESSION["distribuidora"]);
		}
		if($dist == null) $this->Json(array("img" => false, "error_code" => 404, "error" => "Distribuidora inválida"));
		if(empty($dist->logo_id)) $this->Json(array("img" => false, "error_code" => 300, "error" => "Logo não existe"));
		$image = new MagratheaImage($dist->logo_id);
		echo $image->Load()->MaxSize(300, 300)->Code($dist->nome);
	}

	public function SetImage(){
		$dist = unserialize($_SESSION["distribuidora"]);
		$imageId = $_POST["image_id"];
		$dist->logo_id = $imageId;
		$dist->Save();
		$_SESSION["distribuidora"] = serialize($dist);
		$this->Json(true);
	}

	public function CheckSlug($slug){
		$dist = DistribuidoraControl::GetBySlug($slug);
		if($dist == null) $this->Json(false);
		$this->Json(array("id" => $dist->id, "nome" => $dist->nome));
	}

	public function Login(){
		$dist = DistribuidoraControl::Login($_POST["email"], $_POST["password"]);
		if(!empty($dist)){
			$_SESSION["distribuidora"] = serialize($dist);
			$this->Json(array("success" => true));
		} else {
			$ret = "Usuário não encontrado ou senha incorreta. <br/>";
			$ret .= "Caso não esteja cadastrado, <a href='/Distribuidora/Cadastro'>clique aqui</a> para efetuar o cadastro.<br/>";
			$ret .= "Para maiores informações, <a href='/Contato'>entre em contato</a>";
			$this->Json(
				array("success" => false, "message" => $ret)
			);
		}
	}

	private function MethodMeansDistribuidora($args){
		$dist_slug = $args["slug"];
		$dist = DistribuidoraControl::GetBySlug($dist_slug);
		if(empty($dist->id)){
			$this->NotFound();
			die;
		}

		$prod = @$_GET["p"];
		$loggedDist = @unserialize($_SESSION["distribuidora"]);
		if($loggedDist != null){
			if($loggedDist->id == $dist->id)
				$this->Smarty->assign("edit", true);
		} else {
			$dist->UpView();
		}
		$produtos = ProdutoControl::GetByDistribuidora($dist->id);

		$this->Smarty->assign("dist", $dist);
		$this->Smarty->assign("prod", $prod);
		$this->Smarty->assign("produtos", $produtos);
		$this->Smarty->display("quemvende/distribuidora.html");
	}

	public function __call($method_name, $args = array()){
		$args["slug"] = $method_name;
		if(method_exists($this, $method_name)){
			return call_user_func_array(array(&$this, $method_name), $args);
		} else {
			return call_user_func( array(&$this, "MethodMeansDistribuidora"), $args );
		}
	}

}

?>