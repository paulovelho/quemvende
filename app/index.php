<?php

	include("inc/global.php");

	MagratheaController::IncludeAllControllers();
	MagratheaModel::IncludeAllModels();
	include("Services/ClearString.php");

	// plugins
	include("plugins/jquery/load.php");
	include("plugins/bootstrap/load.php");

	include("plugins/font-awesome4/load.php");
	include("plugins/jquery-validation/load.php");
	include("plugins/dropzone/load.php");
	include("plugins/MagratheaImages2/load.php");
	include("plugins/colorbox/load.php");
	// css & javascript
	try{
		MagratheaView::Instance()
			->IncludeCSS("css/animate.css")
			->IncludeCSS("css/templatemo_style.css")
			->IncludeCSS("css/templatemo_misc.css")
			->IncludeCSS("css/style.css")
			->IncludeJavascript("javascript/scripts.js")
			->IncludeJavascript("javascript/contato.js")
			->IncludeJavascript("javascript/distribuidora.js")
			->IncludeJavascript("javascript/estabelecimento.js")
			->IncludeJavascript("javascript/produtos.js")
			->IncludeJavascript("plugins/jquery-validation/localization/messages_pt_BR.min.js");
	} catch(Exception $ex){
		BaseControl::DisplayError($ex);
	}
	
	// set alternative routes:
	$routes = array(
		"Privacidade" => "Home/Privacidade",
		"Termos" => "Home/Termos",
		"EsqueciASenha" => "Home/EsqueciASenha",
		"EsqueciASenha/Send" => "Home/SendSenha"
	);

	MagratheaRoute::Instance()
		->SetRoute($routes)
		->Route($control, $action, $params);

//	p_r(MagratheaRoute::Instance());
//	Debug("loading ".$control."/".$action);

	try{
		// looooooaad!
		BaseControl::Start();
		MagratheaController::Load($control, $action, $params);
	} catch (Exception $ex) {
//		echo $ex;
		// in case of error, it can be treated here. 
		// you can control the 404 error here as well!
		BaseControl::Go404();
	}

?>







